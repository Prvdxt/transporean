package model;

import java.util.List;

public class IssueDetails {
	
	private String title;
	
	private String issuerId;
	
	private String author;
	
	private List<String> labels;
	
	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	private String dateTime;
	
	private String numberOfComments;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	


	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getNumberOfComments() {
		return numberOfComments;
	}

	public void setNumberOfComments(String numberOfComments) {
		this.numberOfComments = numberOfComments;
	}

	

}
