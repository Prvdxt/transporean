package frameworkclass;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class DriverClass {
	public static WebDriver driver;

	@BeforeMethod
	public static WebDriver driverLaunch() {
		System.setProperty("webdriver.chrome.driver", ObjectRepository.CHROMEDRIVERPATH);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		return driver;
	}

	public static void openBrowserTest1() {

		driver.get(ObjectRepository.URL_1);

	}
	
	public static void openBrowserTest2() {
		driver.get(ObjectRepository.URL_2);

	}

	@AfterMethod
	public void closeBrowser() {
		driver.quit();
	}

}
