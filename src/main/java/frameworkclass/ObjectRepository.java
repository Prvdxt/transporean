package frameworkclass;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ObjectRepository {

	public ObjectRepository(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public static String CHROMEDRIVERPATH = System.getProperty("user.dir") + "//Drivers//chromedriver.exe";

	public static String URL_1 = "https://github.com/SeleniumHQ/selenium/issues";
	public static String URL_2 = "https://github.com/SeleniumHQ/selenium/graphs/commit-activity";

	/******** FIRST PAGE ********/

	@FindBy(xpath = "//input[@id='js-issues-search']")
	public WebElement SEARCHFIELD;

	@FindBy(xpath = "(//a[@href='/SeleniumHQ/selenium/labels'])[1]")
	public WebElement LABELLINK;

	@FindBy(xpath = "//input[@placeholder='Search all labels']")
	public WebElement SEARCHALLLABELS;

	@FindBy(xpath = "//a//span[contains(text(),'C-java')]")
	public WebElement CJAVALABEL;

	@FindBy(xpath = "(//a[@data-ga-click='Issues, Table state, Open'])[1]")
	public WebElement OPEN;

	@FindBy(xpath = "(//a[@class='Link--primary v-align-middle no-underline h4 js-navigation-open markdown-title'])[1]")
	public WebElement FIRSTLINK;

	@FindBy(xpath = "((//div[@aria-label='Issues']//div//div)[1]//div//div//following-sibling::div//a)[1]")
	public WebElement TITLE;

	@FindBy(xpath = "(//span[@class='d-inline-block mr-1']//following-sibling::span[1])[1]")
	public List<WebElement> LABELS_OF_LINK;

	@FindBy(xpath = "(//div//a//span[@class='text-small text-bold'])[1]")
	public WebElement NUM_OF_COMMENTS;

	@FindBy(xpath = "(//div[@aria-label='Issues']//div//div)[1]")
	public WebElement ISSUERID;

	@FindBy(xpath = "(//div//span[@class='opened-by']//a)[1]")
	public WebElement AUTHOR;

	@FindBy(xpath = "(//relative-time[@datetime])[1]")
	public WebElement DATE_TIME;

	/********************* SECOND TEST ****************************/

	@FindBy(xpath = "//section[@id='commit-activity-master']")
	public WebElement SECTIONOFGRAPH;

	@FindBy(xpath = "(//section[@id='commit-activity-master']//child::*//child::*//child::*//following-sibling::*//following-sibling::*//following-sibling::*//following-sibling::*//following-sibling::*)[13]")
	public WebElement BAROFGRAPH;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[1]")
	public WebElement COMMITONSUNDAY;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[9]")
	public WebElement COMMITONMONDAY;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[10]")
	public WebElement COMMITONTUESDAY;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[11]")
	public WebElement COMMITONWEDNESSDAY;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[12]")
	public WebElement COMMITONTHURSDAY;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[13]")
	public WebElement COMMITONFRIDAY;

	@FindBy(xpath = "(//section[@id='commit-activity-detail']//child::*//child::*//child::*//following-sibling::*//child::*[2])[14]")
	public WebElement COMMITONSATURDAY;
	
	
	//@FindBy(xpath="(//section[@id='commit-activity-detail']//child::*//child::*//child::*)[7]//child::*")
	@FindBy(xpath="(//section[@id='commit-activity-detail']//child::*//child::*//child::*)[22]")
	public WebElement DAYOFWEEK;

}
