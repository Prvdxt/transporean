package frameworkclass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass {

	public static WebDriverWait wait;


	public static void clickElement(WebDriver driver, WebElement element) {
		

		element.click();
	}

	public static void windowMaximize(WebDriver driver) {
		driver.manage().window().maximize();
	}

	public static void enterTextInBox(WebDriver driver, WebElement element, String text) {
		waitForElementimplicityly(driver);
		element.sendKeys(text);

	}

	

	public static String waitExplicitly(WebDriver driver, WebElement element) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		String str = element.getText();
		return str;
	}

	public static void hoverMouseOverElement(WebDriver driver, WebElement element1) {

		Actions builder = new Actions(driver);
		builder.moveToElement(element1).perform();


	}


	public static boolean checkDisplay(WebDriver driver, WebElement element) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
		boolean check = element.isDisplayed();
		return check;

	}

	public static ArrayList<String> getListOfElements(WebDriver driver, List<WebElement> element) {
		ArrayList<String> s=new ArrayList<String>();
		Iterator<WebElement> i=element.iterator();
		while(i.hasNext()) {
		    WebElement text = i.next();
		    String string=text.getText();
		    s.add(string);}
	
		return s;

	}

	public static void waitForElementimplicityly(WebDriver driver)

	{
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);

	}

	public static void waitForElementInvisibility(WebDriver driver, WebElement element) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.invisibilityOf(element));
	}

	public static void clearATextArea(WebElement webElement) {

		webElement.clear();

	}

	public static void writeToTextBoxAndEnter(WebElement webElement, String text) {

		webElement.click(); 
		webElement.sendKeys(text);
		webElement.sendKeys(Keys.ENTER);
		
		
	}

	public static String getTextofElement(WebDriver driver, WebElement element) {
		
		String text=element.getText();
		return text;
		
		
	}

	public static void waitForElementVisibility(WebDriver driver, WebElement element) {
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		
	}

	public static String getAttribOfElement(WebDriver driver, WebElement element, String attribute) {
		return element.getAttribute(attribute);
	}

	public static void hoverMouseOverAnElement(WebDriver driver, WebElement element) throws InterruptedException {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		waitForElementimplicityly(driver);
		action.doubleClick().build().perform();
		waitForElementimplicityly(driver);
		Thread.sleep(500); 
		
	}

	public static void scrollToElement(WebDriver driver, WebElement element) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500); 
	}

	public static void refreshThePage(WebDriver driver) {
		driver.navigate().refresh();
		
	}
}
