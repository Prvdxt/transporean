package pageclass;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import frameworkclass.BaseClass;
import frameworkclass.DriverClass;
import frameworkclass.ObjectRepository;

public class SecondTest {
	WebDriver driver;

	ObjectRepository objRep;

	public SecondTest(WebDriver driver) {
		this.driver = driver;
		this.objRep = new ObjectRepository(driver);
	}

	public void navigateToPage(WebDriver driver) {

		DriverClass.openBrowserTest2();
		BaseClass.windowMaximize(driver);
		BaseClass.waitForElementimplicityly(driver);

	}

	public void waitforPageToLoad(WebDriver driver2) {
		BaseClass.waitForElementimplicityly(driver);

	}

	public void clickOnTheBar(WebDriver driver) {

		Actions act = new Actions(driver);
		act.doubleClick(objRep.BAROFGRAPH).build().perform();
		System.out.println("Clicked the BAR");

//		Point location=objRep.SECTIONOFGRAPH.getLocation();
//		BaseClass.waitForElementimplicityly(driver);
//
//		System.out.println(location);
//		int x1=location.x+25;
//		int y1=location.y+20;
//		
//		Actions actions = new Actions(driver);
//		int xPosition = objRep.SECTIONOFGRAPH.getSize().width+540;
//		int yPosition = objRep.SECTIONOFGRAPH.getSize().width+540;
//		actions.moveToElement(objRep.SECTIONOFGRAPH,x1,y1);
//		BaseClass.waitForElementimplicityly(driver);
//		actions.moveToElement(objRep.SECTIONOFGRAPH, xPosition, yPosition).contextClick().build().perform();
//		BaseClass.waitForElementimplicityly(driver);
//		System.out.println("Clicked");

	}

	public Map<String,Integer> getCommitsFromDayOfWeek(WebDriver driver) {
		int v1 = Integer.parseInt(objRep.COMMITONMONDAY.getText());
		System.out.println(v1);
		int v2 = Integer.parseInt(objRep.COMMITONTUESDAY.getText());
		System.out.println(v2);
		int v3 = Integer.parseInt(objRep.COMMITONWEDNESSDAY.getText());
		System.out.println(v3);
		int v4 = Integer.parseInt(objRep.COMMITONTHURSDAY.getText());
		System.out.println(v4);
		int v5 = Integer.parseInt(objRep.COMMITONFRIDAY.getText());
		System.out.println(v5);
		int v6 = Integer.parseInt(objRep.COMMITONSATURDAY.getText());
		System.out.println(v6);
		int v7 = Integer.parseInt(objRep.COMMITONSUNDAY.getText());
		System.out.println(v7);
		
		Map<String,Integer> mp= new HashMap<String, Integer>();
		mp.put("SUNDAY", v7);
		mp.put("MONDAY", v1);
		mp.put("TUESDAY", v2);
		mp.put("WEDNESSDAY", v3);
		mp.put("THURSDAY", v4);
		mp.put("FRIDAY", v5);
		mp.put("SATURDAY", v6);
		
		
		return mp;
	}

	public void commitForTheDayOfWeek(WebDriver driver2) {
		
		
	}

	public int getKeyValuesforCommits(Map<String,Integer> mp) {
		int sum =0;
		for(Integer i :mp.values()) {
			sum += i;
		}
		return sum;
	}

	public String getDayOfWeek(WebDriver driver) throws InterruptedException {
		
		BaseClass.scrollToElement(driver, objRep.DAYOFWEEK);
		String s=BaseClass.getTextofElement(driver, objRep.DAYOFWEEK);
		System.out.println(s);
		return s;
	}

}
