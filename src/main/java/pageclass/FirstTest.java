package pageclass;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.fasterxml.jackson.databind.ObjectMapper;

import frameworkclass.BaseClass;
import frameworkclass.DriverClass;
import frameworkclass.ObjectRepository;
import model.IssueDetails;

public class FirstTest {
	WebDriver driver;

	ObjectRepository objRep;

	public FirstTest(WebDriver driver) {
		this.driver = driver;
		this.objRep = new ObjectRepository(driver);
	}

	public void navigateToPage(WebDriver driver) {

		DriverClass.openBrowserTest1();
		BaseClass.windowMaximize(driver);

	}

	public void clearAndEnterContent(WebDriver driver, String search) {

		BaseClass.clearATextArea(objRep.SEARCHFIELD);
		BaseClass.writeToTextBoxAndEnter(objRep.SEARCHFIELD, search);
		WebElement e = driver.findElement(By.xpath("//a[@id='issue_1202_link']"));
		BaseClass.waitExplicitly(driver, e);
		BaseClass.waitForElementimplicityly(driver);
	}

	public void filterthelabel(WebDriver driver, String labelFilter) throws InterruptedException {

		BaseClass.waitForElementVisibility(driver, objRep.LABELLINK);
		BaseClass.clickElement(driver, objRep.LABELLINK);
		BaseClass.waitExplicitly(driver, objRep.CJAVALABEL);
		BaseClass.waitForElementimplicityly(driver);
		BaseClass.scrollToElement(driver, objRep.CJAVALABEL);
		BaseClass.hoverMouseOverAnElement(driver, objRep.CJAVALABEL);
		BaseClass.waitForElementimplicityly(driver);
		System.out.println("Clicked the JAVALabel");
		BaseClass.waitForElementimplicityly(driver);

	}

	public void clickOpenLabels(WebDriver driver) throws InterruptedException {

		Actions action = new Actions(driver);
		action.moveToElement(objRep.OPEN).build().perform();

		action.doubleClick().build().perform();

		Thread.sleep(500);


	}

	public IssueDetails getAttributesOfFirstResult(WebDriver driver) {
		IssueDetails details = new IssueDetails();

		BaseClass.clickElement(driver, objRep.FIRSTLINK);
		String title = BaseClass.getTextofElement(driver, objRep.TITLE);
		details.setTitle(title);
		System.out.println(title);

		ArrayList<String> labels = BaseClass.getListOfElements(driver, objRep.LABELS_OF_LINK);
		details.setLabels(labels);
		System.out.println(labels);

		String numberOfComments = BaseClass.getTextofElement(driver, objRep.NUM_OF_COMMENTS);
		details.setNumberOfComments(numberOfComments);
		System.out.println(numberOfComments);

		String author = BaseClass.getTextofElement(driver, objRep.AUTHOR);
		details.setAuthor(author);
		System.out.println(author);

		String dateTime = BaseClass.getAttribOfElement(driver, objRep.DATE_TIME, "datetime");
		details.setDateTime(dateTime);
		System.out.println(dateTime);

		String issuerId = BaseClass.getAttribOfElement(driver, objRep.ISSUERID, "id");
		details.setIssuerId(issuerId);
		System.out.println(issuerId);

		return details;
	}

	public void waitforPageToLoad(WebDriver driver) {

		BaseClass.waitForElementimplicityly(driver);

	}

	public void hoverOverElement(WebDriver driver) throws InterruptedException {
		BaseClass.hoverMouseOverAnElement(driver, objRep.LABELLINK);

	}

	public void createJSONFileWithIssueDetails(IssueDetails details) {
		
		ObjectMapper mapper = new ObjectMapper();

        File file = new File("most_discussed_issue.json");
        try {
            // Serialize Java object info JSON file.
            mapper.writeValue(file, details);
        } catch (IOException e) {
            e.printStackTrace();
        }

	}
}
