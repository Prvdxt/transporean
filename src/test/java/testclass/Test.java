package testclass;

import java.util.Map;

import org.testng.Assert;

import frameworkclass.DriverClass;
import model.IssueDetails;
import pageclass.FirstTest;
import pageclass.SecondTest;

/**
 * This method is used to test the is displayed.
 * 
 * @author e077512
 *
 */
public class Test extends DriverClass {
	
	

	@org.testng.annotations.Test(priority = 0, enabled = true)
	public void test1() throws InterruptedException {

		String search = "sort:comments-desc";
		String labelFilter = "C-java";

		FirstTest firstTest = new FirstTest(driver);
		firstTest.navigateToPage(driver);
		firstTest.waitforPageToLoad(driver);
		firstTest.clearAndEnterContent(driver,search);
		firstTest.waitforPageToLoad(driver);
		firstTest.filterthelabel(driver,labelFilter);
		firstTest.waitforPageToLoad(driver);
		firstTest.clickOpenLabels(driver);
		firstTest.waitforPageToLoad(driver);
		IssueDetails details = firstTest.getAttributesOfFirstResult(driver);
		firstTest.createJSONFileWithIssueDetails(details);
		

	}
	@org.testng.annotations.Test(priority = 0, enabled = true)
	public void test2() throws InterruptedException {

		

		SecondTest secondTest = new SecondTest(driver);
		secondTest.navigateToPage(driver);
		secondTest.waitforPageToLoad(driver);
		secondTest.clickOnTheBar(driver);
		
		Map<String,Integer> mp=secondTest.getCommitsFromDayOfWeek(driver);
		
		int sum=secondTest.getKeyValuesforCommits(mp);
		System.out.println("Sum :"+sum);
		Assert.assertEquals(sum, 31);
		

		
		Assert.assertEquals((int)mp.get("SUNDAY"), 0);
		System.out.println("Sunday :"+(int)mp.get("SUNDAY"));
		Assert.assertEquals((int)mp.get("MONDAY"), 4);
		Assert.assertEquals((int)mp.get("TUESDAY"), 7);
		Assert.assertEquals((int)mp.get("WEDNESSDAY"), 2);
		Assert.assertEquals((int)mp.get("THURSDAY"), 7);
		Assert.assertEquals((int)mp.get("FRIDAY"), 11);
		Assert.assertEquals((int)mp.get("SATURDAY"), 0);
		
		
		String val= secondTest.getDayOfWeek(driver);
		Assert.assertEquals(val, "Sunday");
		
	}

}
